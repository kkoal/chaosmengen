function Mandelbrot()

    depth = 100;
    pixel = 500;

    x = linspace(-2, 2, pixel);
    y = linspace(-2, 2, pixel)';

    [rp, ip] = meshgrid(x, y);
    cM = rp + i * ip;

    image = zeros(pixel, pixel);

    cN = image;             
    for x = 1:depth
        cN = cN .* cN + cM;       
        image = image + (abs(cN) < 2);   
    end;

    imagesc(image);
    colormap(parula(depth));

    axis equal
    axis off
end
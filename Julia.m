function Julia( rp, ip )

    depth = 100;
    pixel = 500;
    c = rp + i * ip;

    k = max(abs(c), 2);

    r = linspace(-k, k, pixel);

    cN = ones(pixel, 1) * r + i * (ones(pixel, 1) * r)';

    image = zeros(pixel,pixel);

    for x = 1:depth
        image = image + (abs(cN) <= k);
        cN = cN .* cN + ones(pixel, pixel) .* c;
    end;

    imagesc(image);
    colormap(parula(depth));

    hold off;
    axis equal;
    axis off;
    
end
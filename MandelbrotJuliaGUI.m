function varargout = MandelbrotJuliaGUI(varargin)
% MandelbrotJuliaGUI MATLAB code for MandelbrotJuliaGUI.fig
%      MandelbrotJuliaGUI, by itself, creates a new MandelbrotJuliaGUI or raises the existing
%      singleton*.
%
%      H = MandelbrotJuliaGUI returns the handle to a new MandelbrotJuliaGUI or the handle to
%      the existing singleton*.
%
%      MandelbrotJuliaGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MandelbrotJuliaGUI.M with the given input arguments.
%
%      MandelbrotJuliaGUI('Property','Value',...) creates a new MandelbrotJuliaGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MandelbrotJuliaGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MandelbrotJuliaGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MandelbrotJuliaGUI

% Last Modified by GUIDE v2.5 12-Feb-2017 14:52:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MandelbrotJuliaGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @MandelbrotJuliaGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MandelbrotJuliaGUI is made visible.
function MandelbrotJuliaGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MandelbrotJuliaGUI (see VARARGIN)

set(handles.juliaSetting,'Visible','off');

Mandelbrot;

% Choose default command line output for MandelbrotJuliaGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes MandelbrotJuliaGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MandelbrotJuliaGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in plotSelect.
function plotSelect_Callback(hObject, eventdata, handles)
% hObject    handle to plotSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Determine the selected data set.
    str = get(hObject, 'String');
    val = get(hObject,'Value');
    % Set current data to the selected data set.
    switch str{val};
    case 'Mandelbrot-Menge' % User selects peaks.
       Mandelbrot;
       set(handles.juliaSetting,'Visible','off');
    case 'Julia-Menge' % User selects membrane.
       Julia(0,1);
       set(handles.rp,'String','0');
       set(handles.ip,'String','1');
       set(handles.juliaSetting,'Visible','on');
    end
    % Save the handles structure.
    guidata(hObject,handles)
    
    
function updateJulia_Callback(hObject, eventdata, handles)

    rp = str2num(get(handles.rp,'String'));
    ip = str2num(get(handles.ip,'String'));
    if isempty(rp) || isempty(ip)
        warndlg('Bitte nur Zahlen eingeben');
    else
        Julia(rp,ip);
    end

guidata(hObject,handles)


% Hints: contents = cellstr(get(hObject,'String')) returns plotSelect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plotSelect


% --- Executes during object creation, after setting all properties.
function plotSelect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plotSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
